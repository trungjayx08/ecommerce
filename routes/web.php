<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Frontend\HomeController;
use App\Http\Controllers\Frontend\ProductController;
use App\Http\Controllers\Frontend\MemberController;
use App\Http\Controllers\Frontend\BlogController;
use App\Http\Controllers\Frontend\CategoryController;
use App\Http\Controllers\Frontend\BrandController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/', [HomeController::class, "index"]);
Route::get('/product', [ProductController::class, "index"]);
Route::get('/product/add', [ProductController::class, "create"]);
Route::get('/product/edit/{id}', [ProductController::class, "edit"]);
Route::post('/product/edit/{id}', [ProductController::class, "update"]);
Route::get('/product/delete/{id}', [ProductController::class, "delete"]);
Route::post('/product/add', [ProductController::class, "store"]);
Route::get('/product-details/{id}', [ProductController::class, "pro_details"]);
Route::get('/check-out', [App\Http\Controllers\Frontend\CheckoutController::class, "index"]);
Route::get('/checkout',[App\Http\Controllers\Frontend\ProductController::class, "checkout"])->name('checkout');
Route::get('/cart', [App\Http\Controllers\Frontend\CartController::class, "index"]);
Route::get('/member-login', [MemberController::class, "viewlogin"]);
Route::post('/member-login', [MemberController::class, "login"]);
Route::get('/logout', [MemberController::class, "logout"]);
Route::get('/member-register', [MemberController::class, "register"]);
Route::post('/member-register', [MemberController::class, "store"]);
Route::get('/bloglist', [BlogController::class, "show"]);
Route::get('/blog/detail/{id}', [BlogController::class, "detail"])->name('BlogController.detail');
Route::post('/blog/ajax_rate', [\BlogController::class, "ajax_rate"])->name('ajax_rate');
Route::post('/blog/cmt', [BlogController::class, "comment"]);
Route::get('/account', [MemberController::class, "account"]);
Route::post('/account', [MemberController::class, "updateaccount"]);
Route::get('/category', [CategoryController::class, "index"]);
Route::get('/category/add', [CategoryController::class, "add"]);
Route::post('/category/add', [CategoryController::class, "save"]);
Route::get('/category/edit/{id}', [CategoryController::class, "edit"]);
Route::post('/category/edit/{id}', [CategoryController::class, "update"]);
Route::get('/category/delete/{id}', [CategoryController::class, "delete"]);
Route::get('/brand', [BrandController::class, "index"]);
Route::get('/brand/add', [BrandController::class, "add"]);
Route::post('/brand/add', [BrandController::class, "save"]);
Route::get('/brand/edit/{id}', [BrandController::class, "edit"]);
Route::post('/brand/edit/{id}', [BrandController::class, "update"]);
Route::get('/brand/delete/{id}', [BrandController::class, "delete"]);
Route::get('/add-to-cart/{id}', [ProductController::class, "addtocart"])->name('addtocart');
Route::post('/update-cart-up',[ProductController::class, "updatecartup"])->name('updatecartup');
Route::post('/update-cart-down',[ProductController::class, "updatecartdown"])->name('updatecartdown');
Route::post('delete-cart',[ProductController::class, "remove"])->name('delete');
Route::post('search',[ProductController::class, "getSearch"])->name('search');
Route::get('shop', [ProductController::class, "shop"]);
Route::post('search-product',[ProductController::class, "searchs"]);
Route::post('sendmail', [ProductController::class, "sendmail"])->name('sendmail');





Auth::routes();
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/admin/dashboard', [App\Http\Controllers\Admin\DashboardController::class, 'index'])->name('dashboard');
Route::get('/admin/profile', [App\Http\Controllers\Admin\UserController::class, 'index'])->name('user');
Route::get('/admin/profile',[App\Http\Controllers\Admin\UserController::class, 'edit']);
Route::post('/admin/profile', [App\Http\Controllers\Admin\UserController::class, 'update']);

Route::get('/admin/country', [App\Http\Controllers\Admin\CountryController::class, 'index'])->name('country');
Route::get('/country/add', [App\Http\Controllers\Admin\CountryController::class, 'add'])->name('add');
Route::post('/country/add', [App\Http\Controllers\Admin\CountryController::class, "save"]);
Route::get('country/edit/{id}', [App\Http\Controllers\Admin\CountryController::class, "edit"]);
Route::post('country/edit/{id}', [App\Http\Controllers\Admin\CountryController::class, "update"]);
Route::get('/country/delete/{id}', [App\Http\Controllers\Admin\CountryController::class, "delete"]);



Route::get('/admin/blog', [App\Http\Controllers\Admin\BlogController::class, 'index']);
Route::get('/blog/add', [App\Http\Controllers\Admin\BlogController::class, 'add'])->name('add');
Route::post('/blog/add', [App\Http\Controllers\Admin\BlogController::class, "save"]);
Route::get('blog/edit/{id}', [App\Http\Controllers\Admin\BlogController::class, "edit"]);
Route::post('blog/edit/{id}', [App\Http\Controllers\Admin\BlogController::class, "update"]);
Route::get('/blog/delete/{id}', [App\Http\Controllers\Admin\BlogController::class, "delete"]);



