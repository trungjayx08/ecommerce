<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Productupdate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product', function (Blueprint $table) {
            $table->string('catetory')->after('price')->nullable();
            $table->string('brand')->after('catetory')->nullable();
            $table->string('sale')->after('brand')->nullable();
            $table->integer('%')->after('sale')->nullable();
            $table->string('company')->after('%')->nullable();
            $table->string('details')->after('company')->nullable();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
