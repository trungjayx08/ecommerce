<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class cmt extends Model
{
    use HasFactory;
    protected $table = 'comment';
    protected $fillable = ['id_user','id_blog','cmt','avt','name','level'];
    public $timestamps = true;
}
