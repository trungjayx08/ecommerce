<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class history extends Model
{
    use HasFactory;
    protected $table = 'history';
    protected $fillable = ['user_id','name','email','phone','price'];
    public $timestamps = true;
}
