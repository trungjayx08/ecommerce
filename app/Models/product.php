<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class product extends Model
{
    use HasFactory;
    protected $table = 'product';
    protected $fillable = ['name','image','price','category','brand','status','sale','company','details'];
    public $timestamps = true;
    
}

