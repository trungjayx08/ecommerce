<?php

namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\blog;
use App\Models\rate;
use App\Models\cmt;





class BlogController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
       $blog = blog::paginate(3);
       
       return view('frontend.blog-list',compact('blog'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function show($id)
    // {
    //     //
    // }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function detail($id)
    {
        $single = blog::where('id',$id)->get();
        $rating = rate::where('blog_id',$id)->avg('rating');
        $cmt = cmt::where('id_blog',$id)->get();
        $rating = round($rating);
        $user = blog::find($id);
        $previous = blog::where('id','<',$user->id)->max('id');
        $next = blog::where('id','>',$user->id)->min('id');
        return view('frontend.blog-single',compact('single','previous','next','rating','cmt'));
    }
    public function ajax_rate(Request $request)
    {
        if(Auth::check()){
            $userid = Auth::id();
            $data = $request->all();
            $data = [
                'blog_id'=> $rating['blog_id'] = $data['blog_id'],
                'rating' => $rating['rating'] = $data['values'],
                'user_id' => $rating['user_id'] = $userid
            ];
            rate::create($data);
            echo 'thanh cong';
        }
       
    }
    public function comment(Request $request)
    {
        $userid = Auth::id();
        $avt = Auth::user()->avatar;
        $name = Auth::user()->name;
        $data = $request->all();
        // dd($data);
        $data = [
            'id_blog' => $cmt['id_blog'] = $data['id_blog'],
            'id_user' => $cmt['id_user'] = $userid,
            'cmt' =>  $cmt['cmt'] = $data['message'],
            'avt' =>  $cmt['avt'] = $avt,
            'name' => $cmt['name'] = $name,
            'level' => $data['level'],
        ];
        cmt::create($data);  
        return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
