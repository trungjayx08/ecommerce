<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\AddProductRequest;
use App\Models\product;
use App\Models\category;
use App\Models\brand;
use App\Models\history;
use Image;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;



class ProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $product = product::all();
        $category = category::all();
        $brand = brand::all();

        return view('frontend.product', compact('product'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = category::all();
        $brand = brand::all();
        return view('frontend.addproduct', compact('category', 'brand'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = [];
        if ($request->hasfile('image')) {
            foreach ($request->file('image') as $image) {
                $name = $image->getClientOriginalName();
                $name_2 = 'hinh85_' . $image->getClientOriginalName();
                $name_3 = 'hinh329_' . $image->getClientOriginalName();


                $path = public_path('upload/product/' . $name);
                $path2 = public_path('upload/product/' . $name_2);
                $path3 = public_path('upload/product/' . $name_3);

                Image::make($image->getRealPath())->save($path);
                Image::make($image->getRealPath())->resize(50, 70)->save($path2);
                Image::make($image->getRealPath())->resize(200, 300)->save($path3);
                $data[] = $name;
            }
        }
        // dd($data);
        $product = new Product();
        $product->image = json_encode($data);
        $product->name = $request->get('name');
        $product->price = $request->get('price');
        $product->category = $request->get('category');
        $product->brand = $request->get('brand');
        $product->status = $request->get('sale');
        $product->sale = $request->get('form-sale');
        $product->company = $request->get('company');
        $product->details = $request->get('details');
        $product->save();
        return redirect()->to('/product');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = product::where('id', $id)->get();
        $getProducts = product::find($id)->toArray();
        $getArrImage = json_decode($getProducts['image'], true);

        // $edit = product::where('id',$id)
        // ->get();
        return view('frontend.editproduct', compact('getArrImage', 'product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
            $getProducts = product::find($id)->toArray();
            $getArrImage = json_decode($getProducts['image'], true);
            $imgDelete = $request->hinhxoa;

            foreach ($getArrImage as $key => $value) {
                if ((in_array($value, $imgDelete))) {
                    unset($getArrImage[$key]);
                    if (is_file('upload/product/' . $value)) {
                        unlink('upload/product/' . $value);
                    }
                }
            }
            $hinhConlai = array_values($getArrImage);
            $data = [];
            if ($request->hasfile('image')) {
                foreach ($request->file('image') as $image) {
                    $name = $image->getClientOriginalName();
                    $name_2 = 'hinh85_' . $image->getClientOriginalName();
                    $name_3 = 'hinh329_' . $image->getClientOriginalName();
                    $data[] = $name;
                }
            }
            // $image = $file->getClientOriginalName();
            $hinhMoi = array_merge($hinhConlai, $data);
            if (count($hinhMoi) > 3) {
                return redirect()->back()->withErrors('Update product error');
            } else {
                if ($request->hasfile('image')) {
                    foreach ($request->file('image') as $image) {
                        $name = $image->getClientOriginalName();
                        $name_2 = 'hinh85_' . $image->getClientOriginalName();
                        $name_3 = 'hinh329_' . $image->getClientOriginalName();


                        $path = public_path('upload/product/' . $name);
                        $path2 = public_path('upload/product/' . $name_2);
                        $path3 = public_path('upload/product/' . $name_3);

                        Image::make($image->getRealPath())->save($path);
                        Image::make($image->getRealPath())->resize(50, 70)->save($path2);
                        Image::make($image->getRealPath())->resize(200, 300)->save($path3);
                        $data[] = $name;
                    }
                }
                $product = product::find($id);
                $product->image = $hinhMoi;
                $product->name = $request->get('name');
                $product->price = $request->get('price');
                $product->category = $request->get('category');
                $product->brand = $request->get('brand');
                $product->status = $request->get('sale');
                $product->sale = $request->get('form-sale');
                $product->company = $request->get('company');
                $product->details = $request->get('details');
                $product->save();
                return redirect()->to('/product');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }
    public function delete($id)
    {
        product::where('id', $id)->delete();
        return redirect()->to('/product');
    }
    public function pro_details($id)
    {
        $details = product::where('id', $id)->get();
        return view('frontend.pro-details', compact('details'));
    }
    public function addtocart(Request $request, $id)
    {
        $product = product::findOrFail($id);

        $cart = session()->get('cart', []);

        if (isset($cart[$id])) {
            $cart[$id]['qty']++;
        } else {
            $cart[$id] = [
                'name' => $product->name,
                'price' => $product->price,
                'image' => $product->image,
                'qty' => 1,
            ];
        };
        session()->put('cart', $cart);
        // session()->flush();
        // return redirect()->back()->with('Ok','Add to cart succsessfully');
        echo 'Done';
    }
    public function updatecartup(Request $request)
    {
        if ($request->id && $request->qty) {
            $cart = session()->get('cart');
            $cart[$request->id]['qty']++;
            session()->put('cart', $cart);
        }
    }
    public function updatecartdown(Request $request)
    {
        if ($request->id && $request->qty) {
            $cart = session()->get('cart');
            if ($cart[$request->id]['qty'] === 1) {
                unset($cart[$request->id]);
                session()->put('cart', $cart);
            } else {
                $cart[$request->id]['qty']--;
                session()->put('cart', $cart);
            }
        }
    }
    public function remove(Request $request)
    {
        if ($request->id) {
            $cart = session()->get('cart');
            if (isset($cart[$request->id])) {
                unset($cart[$request->id]);
                session()->put('cart', $cart);
            }
        }
    }
    public function getSearch(Request $request)
    {
        // $output = '';
        $product = Product::where('name', 'like', '%' . $request->keyword . '%')
            ->orWhere('price', 'LIKE', '%' . $request->keyword . '%')->get();
        return response()->json($product);
    }
    public function searchs(Request $request)
    {

        if ($request->name) {
            $products = Product::where('name', 'LIKE', '%' . $request->name . '%');
        }
        if ($request->price) {
            $price = $request->price;
            $priceex = explode("-",$price);
            $star = $priceex[0];
            $end = $priceex[1];
            $products = Product::where('price', '>=', $star)
                                ->where('price','<=',$end);
                                
        }
        if ($request->status) {

            $products = Product::where('status', 'LIKE', '%' . $request->status . '%');
        }
        if ($request->category) {

            $products = Product::where('category', 'LIKE', '%' . $request->category . '%');
        }
        if ($request->brand) {

            $products = Product::where('brand', 'LIKE', '%' . $request->brand . '%');
        }
        if (count($request->all()) <= 0) {
            $products = Product::all();
        } else {
            $products = $products->get();
        }
        return response()->json($products);
    }
    public function shop()
    {
        $shop = product::query()->latest()->paginate(15);
        $category = category::all();
        $brand = brand::all();
        return view('frontend.shop', compact('shop', 'category', 'brand'));
    }
    public function checkout(Request $request)
    {
        if (Auth::check()) {
            return redirect()->to('/check-out');;
        } else {
            return redirect()->to('/member-register');
        }
    }
    public function sendmail(Request $request)
    {
        $name = Auth::user()->name;
        $fullname = $request->get('name');
        $to = Auth::user()->email;
        $data = [
            'user_id' => $request->get('user_id'),
            'name' => $request->get('name'),
            'email' => Auth::user()->email,
            'phone' => $request->get('phone'),
            'price' => $request->get('price')
        ];

        Mail::send('frontend.emails.sendmail', compact('name'), function ($email) use ($to, $fullname) {
            $email->subject('SHOPPING LARAVEL');
            $email->to($to, $fullname);
        });
        history::create($data);
        return redirect()->back()->with('Ok', 'Succsessfully');;
    }
}
