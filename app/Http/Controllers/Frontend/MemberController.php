<?php

namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\blog;
use App\Models\User;
use App\Http\Requests\MbRegisRequest;
use App\Http\Requests\MemberLoginRequest;
use App\Http\Requests\AccountRequest;
use Exception;
use Illuminate\Support\Facades\Auth;

class MemberController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function register()
    {
        return view('frontend.register');
       
    }
    public function store(Request $request)
    {
        $result = $request->all();
        $result['password']= bcrypt($request->get('password'));
        $file = $request->file('avatar');
        $result['avt'] = $file->getClientOriginalName();
        $result['level'] = 0;
        try {
            if(User::create($result)){
                $result['avatar']->move('upload/register',$result['avatar']->getClientOriginalName());
            }
        } catch (Exception $th) {
            throw new Exception('ACBCD');
        }
        // return response()->json($user);
       
        
        return redirect()->back()->with('success',__('Register account success'));
    }
    public function viewlogin()
    {
        return view('frontend.login');
       
    }
    public function login(MemberLoginRequest $request)
    {
        $login = [
            'email' => $request->email,
            'password'=> $request->password,
            'level' => 0
        ];
        if(Auth::attempt($login)){
            return redirect('/');
        }else{
            return redirect()->back()->withErrors('Email or Password is not correct');
        }
    }
    
    public function logout()
    {
        Auth::logout();
        return redirect()->to('member-login');
    }
    public function account()
    {
        return view('frontend.account');
    }
    public function updateaccount(AccountRequest $request)
    {
        $userId = Auth::id();


        $user = User::findOrFail($userId);


        $data = $request->all();
        
        $file = $request->file('avatar');
        if(!empty($file)){
            $data['avatar'] = $file->getClientOriginalName();

        }
        if($data['password']){
            $data['password'] = bcrypt($data['password']);
        }else{
            $data['password'] = $user->password;
        }
        // dd($data);
        if($user->update($data)){
            if(!empty($file)){
                $file->move('upload',$file->getClientOriginalName());
            }
            return redirect()->back()->with('success',__('Update profile success'));

        }else{
            return redirect()->back()->withErrors('Update profile error');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function show($id)
    // {
    //     //
    // }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function detail($id)
    {
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
