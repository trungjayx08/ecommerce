
<div class="row">
	<h2>Hi {{$name}}</h2>
	<p>Bạn đã đặt hàng tại hệ thống của chúng tôi,Vui lòng kiểm tra lại thông tin đơn hàng</p>
	<div class="col-12">
		<div class="card">
			<div class="table-responsive">
				<table class="table" border="1" cellspacing="0" cellpadding="10" style="width:60%">
					<thead class="thead-light">
						<tr>
							<th scope="col">Name</th>
							<th scope="col">Email</th>
							<th scope="col">Phone</th>
							<th scope="col">Address</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<th scope="row">{{Auth::user()->name}}</th>
							<td>{{Auth::user()->email}}</td>
							<td>{{Auth::user()->phone}}</td>
							<td>{{Auth::user()->address}}</td>
						</tr>

					</tbody>
				</table>
			</div>
		</div>
	</div>

</div><br></br>
<div class="testemail">
	<div class="table-responsive cart_info">
		<table class="table table-condensed" border="1" cellspacing="0" cellpadding="10" style="width:60%">
			<thead>
				<tr class="cart_menu">
					<td class="image">Item</td>
					<td class="description">Name</td>
					<td class="price">Price</td>
					<td class="quantity">Quantity</td>
					<td class="total">Total</td>
					<td></td>
				</tr>
			</thead>
			<tbody>
				<?php
				$total = 0;
				?>
				@if(session('cart'))
				@foreach(session('cart') as $id => $value)
				<?php
				$getArrImage = json_decode($value['image'], true);
				$str = substr($value['price'], 1);
				$subtotal = $str * $value['qty'];
				$total += $str * $value['qty'];

				?>
				<tr id={{$id}} name="id">
					<td class="cart_product">
						<a href=""><img src="{{ asset('upload/product') }}/{{$getArrImage[0]}}" style="width:100px;height:100px" alt=""></a>
					</td>
					<td class="cart_description">
						<h4><a href="">{{$value['name']}}</a></h4>
						<p>Web ID: {{$id}}</p>
					</td>
					<td class="cart_price">
						<p>{{$value['price']}}</p>
					</td>
					<td class="cart_quantity">
						<div class="cart_quantity_button">
							<button type="submit" style="float: left;" class="cart_quantity_up" href=""> + </button>
							<input class="cart_quantity_input" type="text" name="qty" value="{{$value['qty']}}" autocomplete="off" size="2">
							<button type="submit" class="cart_quantity_down" href=""> - </button>
						</div>
					</td>
					<td class="cart_total">
						<?php
						$price = $value['price'];

						?>
						<p class="cart_total_price">${{$subtotal}}</p>
					</td>
				</tr>
				@endforeach
				@endif



				<tr>
					<td colspan="4">&nbsp;</td>
					<td colspan="2">
						<table class="table table-condensed total-result">
							<tr>
								<td>Cart Sub Total</td>
								<td>$59</td>
							</tr>
							<tr>
								<td>Exo Tax</td>
								<td>$2</td>
							</tr>
							<tr class="shipping-cost">
								<td>Shipping Cost</td>
								<td>Free</td>
							</tr>
							<tr>
								<td>Total</td>
								<td><span>${{$total}}</span></td>
							</tr>
						</table>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>