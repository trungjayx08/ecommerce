@extends('frontend.layouts.app')
@section('content')
<h2 class="title text-center">Features Items</h2>
<div>
        <form action="javascript:void(0)" method="POST" id="search_product">
            @csrf
            <input name="name" id='name' style="border:0 none;background:#F0F0E9;padding:10px;display:inline-block;width:150px" type="text" placeholder="name">
            <select name="price" id="price" style="border:0 none;background:#F0F0E9;padding:10px;display:inline-block;width:150px" id="">
                <option value="">Choose Price</option>
                <option value="10-100">10-100</option>
                <option value="1000-5000">1000-5000</option>
                <option value="5000-10000">5000-10000</option>
            </select>
            <select name="category" id="category" style="border:0 none;background:#F0F0E9;padding:10px;display:inline-block;width:150px" id="">
                <option value="">Category</option>
                @foreach($category as $value)
                <option value="{{$value->category}}">{{$value->category}}</option>
                @endforeach
            </select>
            <select name="brand" id="brand" style="border:0 none;background:#F0F0E9;padding:10px;display:inline-block;width:150px" id="">
                <option value="">Brand</option>
                @foreach($brand as $value)
                <option value="{{$value->brand}}">{{$value->brand}}</option>
                @endforeach
            </select>
            <select name="status" id="status" style="border:0 none;background:#F0F0E9;padding:10px;display:inline-block;width:150px" id="">
                <option value="">Status</option>
                <option value="NEW">NEW</option>
                <option value="SALE">SALE</option>
            </select>
            <button id="search" class="btn btn-warning">Search</button>
        </form>
    </div><br></br>
<div class="features_items">
    <!--features_items-->
  
        @foreach($shop as $value)
        <?php
        $getArrImage = json_decode($value['image'], true);
        // dd($value['image']);
        ?>
        <div class="col-sm-4">
            <div class="product-image-wrapper">
                <div class="single-products">
                    <div class="productinfo text-center">
                        <img src="{{ asset('upload/product') }}/{{$getArrImage[0]}}" alt="" />
                        <h2>${{$value->price}}</h2>
                        <p>{{$value->name}}</p>
                        <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                    </div>
                    <div class="product-overlay">
                        <div class="overlay-content">
                            <h2>${{$value->price}}</h2>
                            <p>{{$value->name}}</p>
                            <button type="submit" class="btn btn-default add-to-cart" id="{{$value->id}}"><i class="fa fa-shopping-cart"></i>Add to cart</button>
                            <a href="{{url('/product-details')}}/{{$value->id}}" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Product-details</a>
                        </div>
                    </div>
                </div>
                <div class="choose">
                    <ul class="nav nav-pills nav-justified">
                        <li><a href="#"><i class="fa fa-plus-square"></i>Add to wishlist</a></li>
                        <li><a href="#"><i class="fa fa-plus-square"></i>Add to compare</a></li>
                    </ul>
                </div>
            </div>
        </div>
        @endforeach


    <script>
        $(document).ready(function() {
            $('button#search').click(function() {
                const data = $('#search_product').serializeArray();
                const obj = {};
                data.map(key => {
                    obj[key.name] = key.value
                })
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "{{url('search-product')}}",
                    method: 'post',
                    data: obj,
                    success: function(response) {
                        // console.log(response);
                        var html = '';
                        for (var pro of response) {
                            var image = JSON.parse(pro['image']);
                            // console.log(image[0]);


                            html += "<div class='col-sm-4'>" +
                                "<div class='product-image-wrapper'>" +
                                "<div class='single-products'>" +
                                "<div class='productinfo text-center'>" +
                                "<img src='{{ asset('upload/product') }}/"+image[0]+"' alt='' />" +
                                "<h2>" + ['$'] + pro.price + "</h2>" +
                                "<p>" + pro.name + "</p>" +
                                "<a href='#' class='btn btn-default add-to-cart'><i class='fa fa-shopping-cart'></i>Add to cart</a>" +
                                "</div>" +
                                "<div class='product-overlay'>" +
                                "<div class='overlay-content'>" +
                                "<h2>" + ['$'] + pro.price  + "</h2>" +
                                "<p>" + pro.name + "</p>" +
                                "<button type='submit' class='btn btn-default add-to-cart' id='" + pro.id + "><i class='fa fa-shopping-cart'></i>Add to cart</button>" +
                                "<a href='{{url('/product-details')}}/" + pro.id + "' class='btn btn-default add-to-cart'><i class='fa fa-shopping-cart'></i>Product-details</a>" +
                                "</div>" +
                                "</div>" +
                                "</div>" +
                                "<div class='choose'>" +
                                "<ul class='nav nav-pills nav-justified'>" +
                                "<li><a href='#'><i class='fa fa-plus-square'></i>Add to wishlist</a></li>" +
                                "<li><a href='#'><i class='fa fa-plus-square'></i>Add to compare</a></li>" +
                                "</ul>" +
                                "</div>" +
                                "</div>" +
                                "</div>";
                        }
                        $('div.features_items').html(html);
                    }
                })
            })
        })
    </script>
    @endsection