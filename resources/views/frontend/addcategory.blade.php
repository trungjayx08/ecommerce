@extends('frontend.layouts.app')
@section('content')
<div class="col-lg-8 col-xlg-9 col-md-7" >
                        <div class="card">
                            <div class="card-body">
                                <form class="form-horizontal form-material" method="post" enctype='multipart/form-data'>
                                    @csrf
                                    <div class="form-group">
                                        <label class="col-md-12">Brand</label>
                                        <div class="col-md-12">
                                            <input type="text" placeholder="Brand" value="" name="category" class="form-control form-control-line">
                                        </div>
										@error('category')
                                            <span style="color:red;">{{$message}}</span></br>
                                    	@enderror     
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <button class="btn btn-success">Add Brand</button>
                                        </div>
                                    </div>
                                 
                                </form>
                            </div>
                        </div>
        </div>
@endsection