@extends('frontend.layouts.app')
@section('content')
<div class="page-breadcrumb">
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="table-responsive">
                            
                                <table class="table">
                                    <thead class="thead-light">
                                        <tr>
                                            <th scope="col">ID</th>
                                            <th scope="col">Category</th>
                                            <th style="width: 7%;">Edit</th>
                                            <th style="width: 10%;">Delete</th>
                                         
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($category as $value)
                                        <tr>
                                            <th scope="row">{{$value->id}}</th>
                                            <td>{{$value->category}}</td>
                                            <td>
                                                <a href="{{url('category/edit',['Id'=>$value->id])}}"><i class="mdi mdi-account-edit"></i>Edit</a>
                                            </td>
                                            <td>
                                                <a href="{{url('category/delete',['Id'=>$value->id])}}"><i class="mdi mdi-delete"></i>Delete</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                          
                                    <!-- <tfoot>
                                        <tr>
                                            <td colspan="8">
                                                <a href="{{url('add')}}"><button id="button" class="btn btn-success" style="float: right;">Thêm cầu thủ</button></a>
                                            </td>
                                        </tr>
                                    </tfoot> -->
                                </table>
                             
                            </div>
                            
                        </div>
                    </div>
                </div>
                <a href="{{url('category/add')}}"><button id="button" style="float: right;" class="btn btn-success">Create category</button></a>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
@endsection