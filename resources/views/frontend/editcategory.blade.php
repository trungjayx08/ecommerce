@extends('frontend.layouts.app')
@section('content')
<div class="col-lg-8 col-xlg-9 col-md-7" >
                        <div class="card">
                            <div class="card-body">
                                <form class="form-horizontal form-material" method="post" enctype='multipart/form-data'>
                                    @csrf
                                    @foreach($edit as $value)
                                    <div class="form-group">
                                        <label class="col-md-12">Category</label>
                                        <div class="col-md-12">
                                            <input type="text" placeholder="Category" value="{{$value->category}}" name="category" class="form-control form-control-line">
                                        </div>
										@error('category')
                                            <span style="color:red;">{{$message}}</span></br>
                                    	@enderror     
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <button class="btn btn-success">Edit Category</button>
                                        </div>
                                    </div>
                                 @endforeach
                                </form>
                            </div>
                        </div>
        </div>
@endsection