@extends('frontend.layouts.app')
@section('content')
<div class="features_items"><!--features_items-->
						<h2 class="title text-center">Features Items</h2>
						@foreach($product as $value)
						<?php
                            $getArrImage = json_decode($value['image'], true);
                            // dd($value['image']);
                        ?>
							
						<div class="col-sm-4">
								<div class="product-image-wrapper">
									
									<div class="single-products">
											<div class="productinfo text-center">
												<img src="{{ asset('upload/product') }}/{{$getArrImage[0]}}" alt="" />
												<h2>{{$value->price}}</h2>
												<p>{{$value->name}}</p>
												<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
											</div>
											<div class="product-overlay">
												<div class="overlay-content">
													<h2>{{$value->price}}</h2>
													<p>{{$value->name}}</p>
													<button type="submit" class="btn btn-default add-to-cart" id="{{$value->id}}"><i class="fa fa-shopping-cart"></i>Add to cart</button>
													<a href="{{url('/product-details')}}/{{$value->id}}" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Product-details</a>
												</div>
											</div>
									</div>
									<div class="choose">
										<ul class="nav nav-pills nav-justified">
											<li><a href="#"><i class="fa fa-plus-square"></i>Add to wishlist</a></li>
											<li><a href="#"><i class="fa fa-plus-square"></i>Add to compare</a></li>
										</ul>
									</div>
								</div>
						</div>
						
						@endforeach
					
					</div><!--features_items-->
					
						
					</div><!--/recommended_items-->
@endsection