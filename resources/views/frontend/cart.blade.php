@extends('frontend.layouts.app')
@section('content')
<section id="cart_items">
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li><a href="#">Home</a></li>
				  <li class="active">Shopping Cart</li>
				</ol>
			</div>
			<div class="table-responsive cart_info">
				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td class="image">Item</td>
							<td class="description"></td>
							<td class="price">Price</td>
							<td class="quantity">Quantity</td>
							<td class="total">Total</td>
							<td></td>
						</tr>
					</thead>
					<tbody>
						<?php
							$total = 0;
						?>
						@if(session('cart'))
							@foreach(session('cart') as $id => $value)
							<?php
								$getArrImage = json_decode($value['image'], true);
								$subtotal = $value['price'] * $value['qty'];
								$total += $value['price']* $value['qty'];	
								
                        	?>
						<tr id={{$id}} name="id">
							<td class="cart_product" >
								<a href=""><img src="{{ asset('upload/product') }}/{{$getArrImage[0]}}" style="width:100px;height:100px" alt=""></a>
							</td>
							<td class="cart_description">
								<h4><a href="">{{$value['name']}}</a></h4>
								<p>Web ID: {{$id}}</p>
							</td>
							<td class="cart_price">
								<p>{{$value['price']}}</p>
							</td>
							<td class="cart_quantity">
								<div class="cart_quantity_button">
									<button type="submit" style="float: left;"  class="cart_quantity_up" href="" > + </button>
									<input class="cart_quantity_input"  type="text" name="qty" value="{{$value['qty']}}" autocomplete="off" size="2">
									<button type="submit" class="cart_quantity_down"  href="" > - </button>
								</div>
							</td>
							<td class="cart_total">
								<?php 
									$price = $value['price'];
								?>
								<p class="cart_total_price">${{$subtotal}}</p>
							</td>
							<td class="cart_delete">
								<button class="cart_quantity_delete" type="submit"><i class="fa fa-times"></i></button>
							</td>
						</tr>
							@endforeach
						@endif
					</tbody>
				</table>
			</div>
		</div>
	</section> <!--/#cart_items-->

	<section id="do_action">
		<div class="container">
			<div class="heading">
				<h3>What would you like to do next?</h3>
				<p>Choose if you have a discount code or reward points you want to use or would like to estimate your delivery cost.</p>
			</div>
			<div class="row">
				<div class="col-sm-6">
					<div class="chose_area">
						<ul class="user_option">
							<li>
								<input type="checkbox">
								<label>Use Coupon Code</label>
							</li>
							<li>
								<input type="checkbox">
								<label>Use Gift Voucher</label>
							</li>
							<li>
								<input type="checkbox">
								<label>Estimate Shipping & Taxes</label>
							</li>
						</ul>
						<ul class="user_info">
							<li class="single_field">
								<label>Country:</label>
								<select>
									<option>United States</option>
									<option>Bangladesh</option>
									<option>UK</option>
									<option>India</option>
									<option>Pakistan</option>
									<option>Ucrane</option>
									<option>Canada</option>
									<option>Dubai</option>
								</select>
								
							</li>
							<li class="single_field">
								<label>Region / State:</label>
								<select>
									<option>Select</option>
									<option>Dhaka</option>
									<option>London</option>
									<option>Dillih</option>
									<option>Lahore</option>
									<option>Alaska</option>
									<option>Canada</option>
									<option>Dubai</option>
								</select>
							
							</li>
							<li class="single_field zip-field">
								<label>Zip Code:</label>
								<input type="text">
							</li>
						</ul>
						<a class="btn btn-default update" href="">Get Quotes</a>
						<a class="btn btn-default check_out" href="">Continue</a>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="total_area">
						<ul>
							<li>Cart Sub Total <span>$59</span></li>
							<li>Eco Tax <span>$2</span></li>
							<li>Shipping Cost <span>Free</span></li>
							<li>Total <span class="total">${{$total}}</span></li>
						</ul>
							<a class="btn btn-default update" href="">Update</a>
							<a class="btn btn-default check_out" href="{{route('checkout')}}">Check Out</a>
					</div>
				</div>
			</div>
		</div>
	</section><!--/#do_action-->
	<script>
		$(document).ready(function(){
			$('button.cart_quantity_up').click(function(){
				var id = $(this).closest('tr').attr('id');
				var qty = $(this).closest('tr').find('input.cart_quantity_input').val();
				var price = $(this).closest('tr').find('td.cart_price').text();
				var price_cut = price.replace('$',"");
				var add = (parseInt(qty)) + 1;
				var total = (parseInt(qty) + 1) * price_cut;
				$(this).closest('tr').find('input.cart_quantity_input').val(add);
				$(this).closest('tr').find('p.cart_total_price').text((['$']+total));
				// console.log(price);
				$.ajax({
					headers: {
							 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					},
					url:"{{route('updatecartup')}}",
					method:'post',
					data:{id:id,qty:qty},
					success:function(data)
					{
						var total1 = $('span.total').text();
						var total_cut = total1.replace('$','');
						var total_area = parseInt(total_cut)+parseInt(price_cut);
						// console.log(tong_cut);
						$('span.total').text((['$']+total_area));
								
					}
				})
							
			})
			$('button.cart_quantity_down').click(function(){
				var thisi = $(this);
				var id = $(this).closest('tr').attr('id');
				var qty = $(this).closest('tr').find('input.cart_quantity_input').val();
				var price = $(this).closest('tr').find('td.cart_price').text();
				var price_cut = price.replace('$',"");
				var down = (parseInt(qty)) - 1;
				var total = (parseInt(qty) - 1) * price_cut;
				$(this).closest('tr').find('input.cart_quantity_input').val(down);
				$(this).closest('tr').find('p.cart_total_price').text((['$']+total));
				// console.log(price);
				$.ajax({
					headers: {
							 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					},
					url:"{{route('updatecartdown')}}",
					method:'post',
					data:{id:id,qty:qty},
					success:function(data)
					{
						
						if(down < 1){
							thisi.closest('tr').remove('tr');
							console.log('aaaaa')
						}
						var tru = $('span.total').text();
						var tru_cut = tru.replace('$','');
						var total_area = parseInt(tru_cut)-parseInt(price_cut);
						// console.log(tong_cut);
						$('span.total').text((['$']+total_area));
								
					},
					error:function(err)
					{
						console.log(err);
					}
				})
							
			})
			$('button.cart_quantity_delete').click(function(){
				var thisi = $(this);
				var id = $(this).closest('tr').attr('id');
				$.ajax({
					headers: {
							 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					},
					url:"{{route('delete')}}",
					method:'post',
					data:{id:id},
					success:function(data)
					{
						thisi.closest('tr').remove('tr')
					}
				})
			})
						
		})				
	</script>
@endsection